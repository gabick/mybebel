<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-Master',
    'version' => 'dev-Master',
    'aliases' => 
    array (
    ),
    'reference' => 'd9855abbbba274267705c0df5e612c543cd69c5c',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-Master',
      'version' => 'dev-Master',
      'aliases' => 
      array (
      ),
      'reference' => 'd9855abbbba274267705c0df5e612c543cd69c5c',
    ),
    'johnpbloch/wordpress' => 
    array (
      'pretty_version' => '5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7a597988102967cdfc28851b6b897d018613823',
    ),
    'johnpbloch/wordpress-core' => 
    array (
      'pretty_version' => '5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '82592ec73d42cf784da38adb0028a24dbacab1b4',
    ),
    'johnpbloch/wordpress-core-installer' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '237faae9a60a4a2e1d45dce1a5836ffa616de63e',
    ),
    'wordpress/core-implementation' => 
    array (
      'provided' => 
      array (
        0 => '5.6.1',
      ),
    ),
  ),
);
