щ�_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:2698;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-12-21 15:25:58";s:13:"post_date_gmt";s:19:"2020-12-21 15:25:58";s:12:"post_content";s:10596:"<!-- wp:paragraph -->
<p>This policy applies to : MyBebel.com</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Last update : October 11<sup>th</sup>, 2020</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The respect of your private life is of the utmost importance for MyBebel, who is responsible for this website. Note also, that the use of the website can be done without opening an account.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>This privacy policy aims to lay out :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>The way your personal information is collected and processed. “Personal information” means any information that could identify you, such as your name, your mailing address, your email address, your location and your IP address ;</li><li>Your rights regarding your personal information ;</li><li>Who is responsible for the processing of the collected and processed information ;</li><li>To whom the information is transmitted ;</li><li>If applicable, the website’s policy regarding cookies</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>This privacy policy complement the terms and conditions that you may find at the following address :</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bebel.gabick.com/terms-and-conditions/">Terms and conditions</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>This privacy policy may be revised or changed at any time. We advise you to consult it regularly as a user of the site. In case of major changes, they will be announced on the website. The new privacy policy will take effect immediately on the first day of posting containing the changes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Collection of personal information</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We collect the following personal information :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Name</li><li>First name</li><li>Mailing address</li><li>Postal code</li><li>Email address</li><li>Phone/fax number</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>The personal information we collect is collected through the collection methods described in the following sections, "Forms and methods of collection" and after.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Forms and methods of collection</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Your personal information is collected through the following methods :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Website registration form</li><li>Order form</li><li>Survey form</li><li>Contest</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>We use the collected data for the following purposes :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Contact</li><li>Managing the website</li><li>Information / Promotional offers</li><li>Statistics</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Interactivity</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Your personal information is also collected through the interactivity between you and the website. This personal information is collected through the following methods :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Comments</li><li>Correspondence</li><li>Information for promotional offers</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>We use the personal information thus collected for the following purpose :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Contact</li><li>Statistics</li><li>Website management</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Cookies and log files</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We collect information through log files and cookies. These allow us to process statistics and information on traffic on the website, to ease navigation and improve your experience for your comfort.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>a) Cookies used by the website</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The cookie files used on the website are the following :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>IP address</li><li>Operating system</li><li>Visited pages and requests</li><li>Date and time of connection</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>The use of such files allows us to achieve the following purposes :</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Improved service and personalized welcome</li><li>Creation of personalized consumption profiles</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>b) Objection to the use of cookies and log files by the website</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>You have the right to object to the recording of these cookies and log files by configuring your web browser.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Once you have disactivated cookies and log files, you may continue your use of the website. However, any malfunction resulting from this deactivation may not be considered of our making.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Sharing personal information</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We are committed not to sell to third parties or generally market the personal information collected. However, it is possible that we share this information with third parties for the following reasons:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Creation of consumption profiles</li><li>Order fulfillment</li><li>Partnership</li><li>Publicity</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>If you do not want your personal information to be shared with third parties, you can object to it at the time of collection or at any time thereafter, as mentioned in the section "Right of objection and withdrawal".</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Storage period of personal information</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The controller will keep in its computer systems, in reasonable security conditions, the entirety of the personal information collected for the following duration : 1 year.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Hosting of personal information</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Our site is hosted by: Gabick</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The information we collect and process is exclusively hosted and processed in Canada.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Controller</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>a) Controller</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The person in charge of processing personal information or the “controller” is : Location My Bebel. The controller may be contacted as follows :</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>MyBebel@info.com</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The controller is in charge of determining the purposes for which personal information is processed and the means at the service of such processing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>b) Obligations for the controller</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The controller is committed to protecting the personal information collected, not transmitting it to third parties without your knowledge, and respecting the purposes for which the information was collected.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In addition, the controller undertakes to notify you in the event of a correction or deletion of personal information, unless this entails disproportionate formalities, costs or procedures for them.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In the event that the integrity, confidentiality or security of your personal information is compromised, the processing manager undertakes to inform you by any means.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Right to object or to withdraw</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>You have the right to object to the website's processing of your personal information ("right to object"). You also have the right to request that your personal information no longer appear, for example, in a mailing list ("right to withdraw").</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If you wish to exercise the right to object or the right to withdraw, you must follow the procedure described hereinafter :</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Users must apply to the controller for their personal information to be restricted by sending an email to maxime.alauzet@gmail.com</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Change to our privacy policy</strong><br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Our privacy policy may be viewed at all times at the following address :</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="/terms-and-conditions/" data-type="URL" data-id="/terms-and-conditions/">Terms and conditions</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We reserve the right to modify our privacy policy in order to guarantee its compliance with the applicable law/</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>You are therefore invited to regularly consult our privacy policy to be informed of the latest changes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>However, in the event of a substantial change to this policy, you will be notified by email using the address you gave us when you registered.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Acceptance of our privacy policy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>By using our website, you certify that you have read and understood this privacy policy and accept its conditions, more specifically conditions relating to the collection and processing of personal information, and the use of cookies.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Applicable law</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We are committed to respect the legislative provisions as specified in :</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Act respecting the protection of personal information in the private sector</em>, icLRQ c P-39.1 ; and/or</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Personal information protection and electronic documents act</em>, SC 2000, c 5.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:14:"Privacy policy";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:16:"privacy-policy-2";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-12-21 15:46:32";s:17:"post_modified_gmt";s:19:"2020-12-21 15:46:32";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:38:"https://bebel.gabick.com/?page_id=2698";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}