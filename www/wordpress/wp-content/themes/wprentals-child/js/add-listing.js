jQuery(function($) {
    $('#new_post').one('submit', function() {
        $(this).find('input[type="submit"]').attr('disabled','disabled');
    });

    $('button[type="submit"]').on('click', function() {
        $(this).attr('disabled','disabled');
        var button = $(this);
        setTimeout(function() {
            console.log('Réactivation du bouton');
            button.attr('disabled', false);
        }, 10000);
    });
});