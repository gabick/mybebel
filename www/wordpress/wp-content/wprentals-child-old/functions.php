<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

    
if ( !function_exists( 'wpestate_chld_thm_cfg_parent_css' ) ):
   function wpestate_chld_thm_cfg_parent_css() {

    $parent_style = 'wpestate_style'; 
    wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.css', array(), '1.0', 'all');
    wp_enqueue_style('bootstrap-theme',get_template_directory_uri().'/css/bootstrap-theme.css', array(), '1.0', 'all');
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css',array('bootstrap','bootstrap-theme'),'all' );
    wp_enqueue_style( 'wpestate-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
   wp_enqueue_style( 'gabick-copyright', '/pub/build/styles/copyright.css' );
   }    
    
endif;
add_action( 'wp_enqueue_scripts', 'wpestate_chld_thm_cfg_parent_css' );
load_child_theme_textdomain('wprentals', get_stylesheet_directory().'/languages');
// END ENQUEUE PARENT ACTION


function taxonomy_scripts() {
    // Register the script
    wp_register_script( 'taxonomy-custom-script', get_stylesheet_directory_uri(). '/js/taxonomy-custom.js', array('jquery'), false, true );

    // Localize the script with new data
    $script_data_array = array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'load_taxonomy' ),
    );
    wp_localize_script( 'taxonomy-custom-script', 'blog', $script_data_array );

    // Enqueued script with localized data.
    wp_enqueue_script( 'taxonomy-custom-script' );
}

/**
 * Enqueue a script with jQuery as a dependency.
 */
function wpdocs_scripts_method() {
    wp_enqueue_script( 'add-listing-custom-script', get_stylesheet_directory_uri() . '/js/add-listing.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method' );

add_action( 'wp_enqueue_scripts', 'taxonomy_scripts' );

add_action('wp_ajax_get_taxonomy_by_ajax', 'get_taxonomy_by_ajax_callback');
add_action('wp_ajax_nopriv_get_taxonomy_by_ajax', 'get_taxonomy_by_ajax_callback');

function get_taxonomy_by_ajax_callback() {
    check_ajax_referer('load_taxonomy', 'security');

    $taxonomy = 'property_action_category';
    // Small hack because the addListing french not connected always return 'ICL_LANGUAGE_CODE = en'
    $original_lang = ICL_LANGUAGE_CODE;
    if(isset($_COOKIE['wp-wpml_current_language'])) {
        $original_lang = $_COOKIE['wp-wpml_current_language']; // Save the current language
    }
    global $sitepress;
    $sitepress->switch_lang($original_lang); // Switch to new language

    // Query the terms in new language instead of current language
    $parents  = get_terms( $taxonomy, array(  "hide_empty" => 0, 'taxonomy' => $taxonomy, 'parent' => 0) );
    $mainCategoryId = $_POST['taxonomy_id'];
    $secondCategoryId = null;
    $firstParent = null;

    $relatedTaxonomy = [
        [
            'primary_Name' => 'boats',
            'primary_ID' => 44,
            'secondary_ID' => 54
        ],
        [
            'primary_Name' => 'motocycles',
            'primary_ID' => 45,
            'secondary_ID' => 55
        ],
        [
            'primary_Name' => 'snowmobiles',
            'primary_ID' => 46,
            'secondary_ID' => 56
        ],
        [
            'primary_Name' => 'atv',
            'primary_ID' => 47,
            'secondary_ID' => 53
        ],
        [
            'primary_Name' => 'bateaux',
            'primary_ID' => 104,
            'secondary_ID' => 84
        ],
        [
            'primary_Name' => 'motocyclettes',
            'primary_ID' => 105,
            'secondary_ID' => 94
        ],
        [
            'primary_Name' => 'motoneiges',
            'primary_ID' => 106,
            'secondary_ID' => 100
        ],
        [
            'primary_Name' => '4 roues',
            'primary_ID' => 103,
            'secondary_ID' => 81
        ],

    ];

    foreach($relatedTaxonomy as $simpleArray) {
        $key = array_search($mainCategoryId,  $simpleArray);
        if ($key !== false) {
            $secondCategoryId = $simpleArray['secondary_ID'];
        }
    }

    if (isset($mainCategoryId) && $parents) {
        foreach ( $parents as $term ) {
            if ($term->term_id == $secondCategoryId) {
                $firstParent = $term;
            }
        }
        ?>
        <?php
    }
    if ($parents) :
        ?>
        <select name="prop_action_category" id="prop_action_category_submit" class="select-submit2">
            <?php
                $childs = get_term_children( $firstParent->term_id, $taxonomy );
                if (count($childs) > 0) {
                    foreach ( $childs as $child ) {
                        $term = get_term_by( 'id', $child, $taxonomy );
                     ?>
                     <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                        <?php
                    }
                } else {
                    ?>
                    <option value="<?php echo $firstParent->term_id; ?>"><?php echo $firstParent->name; ?></option>
                    <?php
                }
                ?>
        </select>
    <?php endif;
}

add_action( 'wp_insert_post', 'my_duplicate_on_publish' );
function my_duplicate_on_publish( $post_id ) {
    global $post;

    // don't save for autosave
    if (is_null($post))
    {
        return $post_id;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }

    // dont save for revisions
    if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
        return $post_id;
    }

    // we need this to avoid recursion see add_action at the end
    remove_action( 'wp_insert_post', 'my_duplicate_on_publish' );

    // make duplicates if the post being saved:
    // #1. is not a duplicate of another or
    // #2. does not already have translations

    $is_translated = apply_filters( 'wpml_element_has_translations', '', $post_id, $post->post_type );

    if ( !$is_translated ) {
        do_action( 'wpml_admin_make_post_duplicates', $post_id );
    }

    // must hook again - see remove_action further up
    add_action( 'wp_insert_post', 'my_duplicate_on_publish' );
}

function modify_user_table( $column ) {
    $column['expiration'] = 'Package Activation';
    $column['listing_available'] = 'Listing available';
    return $column;
}
add_filter( 'manage_users_columns', 'modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'expiration' :
            return get_the_author_meta( 'package_activation', $user_id );
        case 'listing_available' :
            return get_the_author_meta( 'package_listings', $user_id );
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

function loginSetRemainingListing( $user_login, $user ) {
    $user_id = $user->ID; // get user id
    // your next code
    $memberShipPackage = get_user_meta($user_id, 'package_id', true);
    $memberShipPackageNbListing = get_post_meta($memberShipPackage, 'pack_listings');

    if($memberShipPackageNbListing !== false) {
        $allowedPost = $memberShipPackageNbListing[0];
    } else {
        $allowedPost = esc_html( wprentals_get_option('wp_estate_free_mem_list',''));
    }

    $args = array(
        'post_type' => 'estate_property',
        'author__in'=> array($user_id), //Authors's id's you like to include
        'suppress_filters' => false,
        'post_status' => array('publish', 'pending') // exclude expired
    );

    $loop = new WP_Query( $args );
    $numberOfPostAlreadyPosted = count($loop->posts);

    $remainingListing = intval($allowedPost) - intval ($numberOfPostAlreadyPosted);

//    if ($remainingListing < 0 ) {
//        $remainingListing = $allowedPost;
//    }

    update_user_meta( $user_id, 'package_listings', $remainingListing) ;

}
add_action( 'wp_login', 'loginSetRemainingListing', 10, 2 );

add_action( 'vc_after_init', 'change_vc_button_colors' );

function change_vc_button_colors() {

    //Get current values stored in the color param in "Call to Action" element
    $param = WPBMap::getParam( 'vc_btn', 'color' );

    // Add New Colors to the 'value' array
    // btn-custom-1 and btn-custom-2 are the new classes that will be
    // applied to your buttons, and you can add your own style declarations
    // to your stylesheet to style them the way you want.
    $param['value'][__( 'Bebel Brand', 'gabick-text-domain' )] = 'btn-custom-brand';

    // Remove any colors you don't want to use.
//    unset($param['value']['Classic Grey']);
//    unset($param['value']['Classic Blue']);
//    unset($param['value']['Classic Turquoise']);
//    unset($param['value']['Classic Green']);
//    unset($param['value']['Classic Orange']);
//    unset($param['value']['Classic Red']);
//    unset($param['value']['Classic Black']);
//    unset($param['value']['Blue']);
//    unset($param['value']['Turquoise']);
//    unset($param['value']['Pink']);
//    unset($param['value']['Violet']);
//    unset($param['value']['Peacoc']);
//    unset($param['value']['Chino']);
//    unset($param['value']['Mulled Wine']);
//    unset($param['value']['Vista Blue']);
//    unset($param['value']['Black']);
//    unset($param['value']['Grey']);
//    unset($param['value']['Orange']);
//    unset($param['value']['Sky']);
//    unset($param['value']['Green']);
//    unset($param['value']['Juicy pink']);
//    unset($param['value']['Sandy brown']);
//    unset($param['value']['Purple']);
//    unset($param['value']['White']);

    // Finally "update" with the new values
    vc_update_shortcode_param( 'vc_btn', $param );
}

#add_filter( 'logout_url', 'my_logout_page', 10, 2 );
#function my_logout_page( $logout_url, $redirect ) {
    #return home_url( '/bebel-modif/?redirect_to=' . $redirect );
#}