jQuery(function($) {
    var firstCategoryField = $('#prop_category_submit');
    var secondCategoryField = $('#prop_action_category_submit');
    function initSpinner() {
        console.log('test');
        secondCategoryField.hide(); // Hide the field on init
        secondCategoryField.prev('label').hide();
        $('body').prepend(
        '<div class="spinner-wrapper">'+
                '<div class="spinner">' +
                    '<div class="bounce1"></div>' +
                    '<div class="bounce2"></div>' +
                    '<div class="bounce3"></div>' +
                '</div>' +
            '</div>'
        );
    }

    function hideSpinner() {
        $('.spinner-wrapper').remove();
    }

    function ajaxCall(data = null) {
        $.ajax({
            type: "POST",
            url: blog.ajaxurl,
            data: data,
            beforeSend: function() {
                console.log("init");
                initSpinner();
            },
            success: function(data) {
                $oldValue = null;
                if($('body').hasClass('page-template-user_dashboard_edit_listing') && secondCategoryField.val() !== '-1') {
                    $oldValue = secondCategoryField.val();
                }
                secondCategoryField.html(data);
                secondCategoryField.prev('label').show();
                secondCategoryField.show();

                if($('body').hasClass('page-template-user_dashboard_edit_listing') && $oldValue !== null) {
                    $selector = '#prop_action_category_submit option[value=dummyValue]';
                    $newSelector = $selector.replace('dummyValue', $oldValue);
                    $($newSelector).prop('selected', 'selected')
                }
            },
            complete: function(data) {
                hideSpinner();
            }
        });
    }

    if($('body').hasClass('page-template-user_dashboard_add_step1-php')) {
        secondCategoryField.hide(); // Hide the field on init
        secondCategoryField.prev('label').hide();

        $('body').on('change', '#prop_category_submit', function() {
            var valueid = $(this).val();
            if(valueid != '') {
                var data = {
                    'action': 'get_taxonomy_by_ajax',
                    'taxonomy_id': valueid,
                    'security': blog.security
                };
                ajaxCall(data);
            }
        });
    };

    if($('body').hasClass('page-template-user_dashboard_edit_listing')) {
        if(firstCategoryField.val() !== '' || firstCategoryField.val() !== '-1') {
            var data = {
                'action': 'get_taxonomy_by_ajax',
                'taxonomy_id': firstCategoryField.val(),
                'security': blog.security
            };
            ajaxCall(data);
        }

        $('body').on('change', '#prop_category_submit', function() {
            var valueid = $(this).val();
            if(valueid != '') {
                var data = {
                    'action': 'get_taxonomy_by_ajax',
                    'taxonomy_id': valueid,
                    'security': blog.security
                };
                ajaxCall(data);
            }
        });
    }

});