��    U      �      l      l     m     }     �     �     �     �  	   �     �     �     �     �     �  /        6     L  
   Q     \     u     �     �     �  �   �  �   �  �   /  [   �  q   (	    �	  o   �
  �   '  �   �  ]   �      [   ;  j   �  W     �   Z  �     �   �  �   �  �   ;  W   �  R     �   i     T     Z     s     |  	   �     �     �     �     �     �     �     �     �     �               +  %   C     i    q     w     �     �  <   �  !   �  <   �     :     H  @   N  �   �      �  ,   �  
   �  N   �  �   7     �     �      �        !   "      D      e     f  	   z     �     �     �     �     �     �     �     �     �     �  6   �     3     M     S  $   [  #   �  $   �     �  !   �  �   
  �   �  �   �  e   S  m   �    '  {   A   �   �   d   �!  o   "  N  {"  f   �#  o   1$  c   �$  �   %  �   �%  �   �&  �   �'  �   j(  V   �(  Q   H)  �   �)     z*     �*     �*  	   �*     �*     �*     �*     �*     �*     �*     �*     
+     +     2+     J+     e+     z+  '   �+     �+  /  �+     �,     �,     -  I   -  '   a-  P   �-     �-     �-  a   �-     T.  #   u/  -   �/     �/  [   �/  �   20     �0     �0     �0     �0     �0     �0   Add New Listing Bookings Cancellation Policy Change your Package Check In Check-In Check-Out City (mandatory) Content / Contenue County Details Details  Email already exists.  Please choose a new one. Email field is empty! Free Front page Go to Calendar settings. Go to Details settings. Go to Location settings. Go to Media settings. Go to Price settings. Hi there,
An invoice was generated for your booking request on %website_url !  A deposit will be required for booking to be confirmed. For more details check out your account, My Reservations page. Hi there,
One of the unconfirmed booking requests you received on %website_url  was cancelled! The request is automatically deleted from your account! Hi there,
One of your booking requests sent on %website_url was rejected by the owner. The rejected reservation is automatically removed from your account.  Hi there,
One of your confirmed bookings on %website_url  was cancelled by property owner.  Hi there,
One of your free listings on  %website_url has expired. The listing is %expired_listing_url.
Thank you! Hi there,
Somebody confirmed a booking on %website_url! You should go and check it out!Please remember that the confirmation is made based on the payment confirmation of a non-refundable fee of the total invoice cost, processed through %website_url and sent to website administrator.  Hi there,
We charged your account on %merchant for a subscription on %website_url ! You should go check it out. Hi there,
We remind you that you need to fully pay the invoice no %invoice_id until  %until_date. This invoice is for booking no %booking_id on property %property_title with the url %property_url.
Thank you. Hi there,
Welcome to %website_url ! You can login now using the below credentials:
Username:%user_login_register
Password: %user_pass_register
If you have any problems, please contact me.
Thank you! Hi there,
You confirmed a booking on %website_url! The booking was confirmed with no deposit! Hi there,
You downgraded your subscription on %website_url. Because your listings number was greater than what the actual package offers, we set the status of all your listings to expired. You will need to choose which listings you want live and send them again for approval.
Thank you! Hi there,
You have a new featured submission on  %website_url ! You should go check it out. Hi there,
You have a new message on %website_url! You should go and check it out!
The message is:
%content Hi there,
You have a new paid submission on  %website_url ! You should go check it out. Hi there,
You have booked a period for your own listing on %website_url !  The reservation will appear in your account, under My Bookings. 
The property is: %booking_property_link Hi there,
You have received a new booking request on %website_url !  Go to your account in Bookings page to see the request, issue the invoice or reject it!
The property is: %booking_property_link Hi there,
You received a new Wire Transfer payment request on %website_url.
The invoice number is:  %invoice_no,  Amount: %total_price.
Please wait until the payment is made to activate the user purchase. Hi there,
Your booking made on %website_url was confirmed! You can see all your reservations by logging in your account and visiting My Reservations page. Hi there,
Your listing, %property_title was approved on  %website_url ! The listing is: %property_url.
You should go check it out. Hi there,
Your new membership on %website_url is activated! You should go check it out. Hi there,
Your purchase on  %website_url is activated! You should go check it out. Hi there,
Your subscription on %website_url was cancelled because it expired or the recurring payment from the merchant was not processed. All your listings are no longer visible for our visitors but remain in your account.
Thank you. Label Listing location details Listings Location Location  Log Out My Bookings My Listings Other Rules Price details: Property Address Property Country Property County Property State Property Zip Property/Item City Search by listing name. Selected dates / Date Sélectionnées Sign Up Someone requested that the password be reset for the following account:
%website_url 
Username: %forgot_username .
If this was a mistake, just ignore this email and nothing will happen. To reset your password, visit the following address:%reset_link,
Thank You! Splash Page State Terms and Conditions Thank you. Please check your email for payment instructions. The email field cannot be blank.  The email was not saved because it is used by another user.  Time Period:  Value We have just sent you an email with Password reset instructions. We received your Wire Transfer payment request on  %website_url !
Please follow the instructions below in order to start submitting properties as soon as possible.
The invoice number is: %invoice_no, Amount: %total_price. 
Instructions:  %payment_details. You already reviewed this bebel! You must to agree with terms and conditions! Your Email Your message was sent! You will be notified by email when a reply is received. Your new password for the account at: %website_url: 
Username:%user_login, 
Password:%user_pass
You can now login with your new password at: %website_url my state value wp_estate_custom_search_Check In wp_estate_custom_search_Check-In wp_estate_custom_search_Check-Out wp_estate_custom_search_my state  Ajouter une annonce Locations Politique d’annulation Changez votre forfait Arrivée Arrivée Départ Ville Content / Contenu Pays Détails Détails L'email existe déjà. Veuillez en choisir un nouveau. Le champ e-mail est vide! Libre Accueil Allez aux Paramètres du calendrier. Allez aux Paramètres des détails. Allez aux Paramètres d'emplacement. Allez aux Paramètres médias. Accéder aux Paramètres de prix. Salut,
Une facture a été générée pour votre demande de réservation sur %website_url! Un acompte vous sera demandé pour la réservation à confirmer. Pour plus de détails, consultez votre compte, page Mes réservations. Salut,
L'une des demandes de réservation non confirmées que vous avez reçues sur %website_url a été annulée! La demande est automatiquement supprimée de votre compte! Salut,
L'une de vos demandes de réservation envoyées sur %website_url a été rejetée par le propriétaire. La réservation refusée est automatiquement supprimée de votre compte. Salut,
L'une de vos réservations confirmées sur %website_url a été annulée par le propriétaire. Salut,
L'une de vos annonces gratuites sur %website_url a expiré. L'annonce est %expired_listing_url.
Merci! Salut,
Quelqu'un a confirmé une réservation sur %website_url ! N'oubliez pas que la confirmation est effectuée sur la base de la confirmation de paiement des frais non remboursables du coût total de la facture, traitée via %website_url et envoyée à l'administrateur du site. Salut,
Nous avons facturé votre compte sur %merchant pour un abonnement sur %website_url! Vous devriez aller le vérifier. Salut,
Nous vous rappelons que vous devez payer intégralement la facture no %facture_id jusqu'au %until_date. Cette facture concerne la réservation no %booking_id sur la propriété %property_title avec l'url %property_url.
Merci. Salut,
Bienvenue sur %website_url !
Si vous rencontrez des problèmes, veuillez me contacter.
Merci! Salut,
Vous avez confirmé une réservation sur %website_url ! La réservation a été confirmée sans acompte! Salut,
Vous avez rétrogradé votre abonnement sur %website_url. Étant donné que le nombre de vos annonces était supérieur à celui proposé par le votre forfait, nous définissons le statut de toutes vos annonces comme expiré. Vous devrez choisir les annonces que vous souhaitez diffuser et les renvoyer pour approbation.
Merci! Salut,
Vous avez une nouvelle soumission en vedette sur %website_url! Vous devriez aller le vérifier. Salut,
Vous avez un nouveau message sur %website_url! Vous devriez aller le vérifier!
Le message est:
%content Salut,
Vous avez une nouvelle soumission payante sur %website_url! Vous devriez aller le vérifier. Salut,
Vous avez réservé une période pour votre propre annonce sur %website_url ! La réservation apparaîtra dans votre compte, sous Mes réservations.
La propriété est: %booking_property_link Salut,
Vous avez reçu une nouvelle demande de réservation sur %website_url! Accédez à votre compte dans la page Réservations pour voir la demande, émettre la facture ou la rejeter!
La propriété est: %booking_property_link Salut,
Vous avez reçu une nouvelle demande de paiement par virement bancaire sur %website_url.
Le numéro de facture est: %no_ facture, Montant: %prix_total.
Veuillez attendre que le paiement soit effectué pour activer l'achat de l'utilisateur. Salut,
Votre réservation effectuée sur %website_url a été confirmée! Vous pouvez voir toutes vos réservations en vous connectant à votre compte et en visitant la page Mes réservations. Salut,
Votre fiche, %property_title a été approuvée sur %website_url! La liste est: %property_url.
Vous devriez aller le vérifier. Votre nouvel abonnement sur %website_url est activé! Vous devriez aller le vérifier. Salut,
Votre achat sur %website_url est activé! Vous devriez aller le vérifier. Salut,
Votre abonnement sur %website_url a été annulé, car il a expiré ou le paiement récurrent n'a pas été traité. Toutes vos annonces ne sont plus visibles pour nos visiteurs mais restent dans votre compte.
Merci. Objet Localisation Annonces Adresse 2 Adresse Déconnexion Mes Locations Mes bébelles Autres règles Détails des prix: Adresse de la bébelle Pays de la bébelle Pays de la bébelle Province de la bébelle Code postal de la bébelle Ville de la bébelle Rechercher par nom Selected dates / Dates sélectionnées  Inscription Quelqu'un a demandé que le mot de passe soit réinitialisé pour le compte suivant:
%website_urk
Nom d'utilisateur: %forgot_username.
Si c'était une erreur, ignorez simplement cet e-mail et rien ne se passera. Pour réinitialiser votre mot de passe, accédez à l'adresse suivante: %reset_link,
Merci! Accueil Province Termes et conditions Merci. Veuillez vérifier votre e-mail pour les instructions de paiement. Le champ e-mail ne peut pas être vide. L'e-mail n'a pas été enregistré car il est utilisé par un autre utilisateur. Durée:  Spécification Nous venons de vous envoyer un e-mail avec les instructions de réinitialisation du mot de passe. Nous avons reçu votre demande de paiement par virement bancaire sur %website_url!
Veuillez suivre les instructions ci-dessous afin de commencer à soumettre des propriétés dès que possible.
Le numéro de facture est: %no_facture, Montant: %total_price.
Instructions: %payment_details. Vous avez critiqué cette Bébelle! Vous devez accepter les termes et conditions! Votre courriel Votre message a été envoyé! Vous serez averti par e-mail lorsqu'une réponse est reçue. Votre mot de passe pour le compte à %website_url a bien été effectué.
Vous pouvez maintenant vous connecter avec votre nouveau mot de passe à:% website_url bobobob valeur Arrivée Arrivée Départ bobobob 