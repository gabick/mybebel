��    *      l      �      �     �     �  	   �  "   �          #     0     D     M     [     w     �     �  ^  �  *    (   A     j     p     �     �     �     �     �     �     �     �     �               $     7     F     V     _     n  �  �     	     	     -	     3	  
   A	      L	     M	     b	     v	  +   	     �	     �	     �	     �	     
     
     :
     N
     k
  �  ~
  !    ,   1     ^     d     v     �     �     �     �     �     �     �          %     @     S     f       
   �     �  "   �  �  �     �     �     �     �     �     Unlimited listings  Featured listings  Listings A password will be e-mailed to you Add New Searches Add Searches Cancellation Policy Check-In Edit Searches Featured Listings remaining Featured included Featured listings included Featured remaining Hi there,
                                You downgraded your subscription on %website_url. Because your listings number was greater than what the actual package offers, we set the status of all your listings to expired. You will need to choose which listings you want live and send them again for approval.
                                Thank you! Hi there,
                                Your subscription on %website_url was cancelled because it expired or the recurring payment from the merchant was not processed. All your listings are no longer visible for our visitors but remain in your account.
                                Thank you. How many Featured listings are included? Label Listings Included Log Out My Listings New Searches No Searches found Parent Searches Price details: Property Country Property County Property State Property Zip Remaining Listings Remaining listings Search Details Search Searches Searches Select package Show Terms and Conditions? Someone requested that the password be reset for the following account:
                                %website_url
                                Username: %forgot_username .
                                If this was a mistake, just ignore this email and nothing will happen. To reset your password, visit the following address:%reset_link,
                                Thank You! Splash Page Unlimited listings Value View Searches Your Email  Annonces illimitées Annonces en vedette Annonces Un mot de passe vous sera envoyé par email Ajouter de nouvelles recherches Ajouter des recherches Politique d’annulation Arrivée Modifier les recherches Annonces en vedette restantes En vedette incluses Annonces en vedette incluses En vedette restant Salut,
                                 Vous avez rétrogradé votre abonnement sur %website_url. Étant donné que le nombre de vos annonces était supérieur à celui proposé par le package réel, nous définissons le statut de toutes vos annonces comme expiré. Vous devrez choisir les annonces que vous souhaitez diffuser et les renvoyer pour approbation.
                                 Merci! Salut,
                                 Votre abonnement sur %website_url a été annulé, car il a expiré ou le paiement récurrent n'a pas été traité. Toutes vos annonces ne sont plus visibles pour nos visiteurs mais restent dans votre compte.
                                 Merci. Combien d'annonces en vedette sont incluses? Objet Annonces incluses Déconnexion Mes bébelles Nouvelle recherche Aucune recherche trouvée Recherches parentes Détails des prix: Pays de la bébelle Pays de la bébelle Province de la bébelle Code postal de la bébelle Annonces Restantes Annonces restantes Détails de la recherche Recherches de recherche Recherches Sélectionner un forfait Afficher les termes et conditions? Quelqu'un a demandé que le mot de passe soit réinitialisé pour le compte suivant:
                                %website_url
                                Nom d'utilisateur: %forgot_username .
                                Si c'était une erreur, ignorez simplement cet e-mail et rien ne se passera. Pour réinitialiser votre mot de passe, visitez l'adresse suivante: %reset_link,
                                Merci Accueil Annonces illimitées Valeur Voir les recherches Votre courriel 